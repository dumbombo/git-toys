Here is the small repo of git-related stuff
===========================================

There are some git settings and pre-commit hook, not so powerful as pre-commit.com

.gitconfig
----------

```ini
[core]
    excludesFile  = /path/to/main/.gitignore
    hooksPath = /path/to/directory/with/hooks

[status]
    showUntrackedFiles = all
```

pre-commit hook
---------------

Iterate git diff, stop working on widely known binaries and executables other than scripts, then analyze, lint each file using corresponding tool, like pylint, mdl, yamllint, etc.

For files other than scripts, markdown and yaml check trailing spaces, missing final empty line.

todo
----

- ✓ make it work
- ✘ invent some settings
- ✓ publish
- _ add features
